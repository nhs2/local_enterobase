from local_entero import create_app
from flask_script import Manager

import os

local_app,c=create_app('development')
manager = Manager(local_app)
from config import write_app_pass




@manager.command
@manager.option('-p', '--password', help='set local system password')
def set_local_password(password=None):
    '''
    set local password to be used for system configuration and test upload files when the adminstrator is not
    login using Warwick EnteroBase
    :param password:
    :return:
    '''
    if not password:
        print ("No password is provided")
        return

    write_app_pass(os.path.expanduser('~'),password)






@manager.command
def run_app():
    '''
    Run the development server
    '''
    # ssl certificate files
    cert=os.path.join(os.path.expanduser('~'),"certs/cert.pem")
    key = os.path.join(os.path.expanduser('~'), "certs/key.pem")

    from local_entero import local_app
    local_app.run(host='0.0.0.0',port=5566, ssl_context=(cert,key))

if __name__ == '__main__':
#    Command.capture_all_args = True
    manager.run()