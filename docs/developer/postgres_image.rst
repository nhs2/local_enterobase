PostgreSQL Image
----------------

Required files and folders
==========================

In "local_enterobase/Singularity_Images/PostgreSQL" sub-folder

* Recipe file: **postgres_simple.def**

Content of the recipe file: postgres_simple.def
===============================================

* The first two lines in the def file pull postgres:alpine docker image

::

  BootStrap: docker
  From: postgres:alpine

* The following section run the image as a singularity instance. It accepts the arguments from the user at run time when starting the service:

::

  %startscript
  docker-entrypoint.sh "$@"

Building PostgreSQL image
=========================
* Assuming that the local_enterobase repository is saved in the sub-folder "local_enterobase" from the current working folder, run the following command to build the PostgreSQL singularity image whihc its name is "postgres_container.sif":

::

  sudo singularity build postgres_container.sif local_enterobase/Singularity_Images/PostgreSQL/postgres_simple.def

Running PostgrSQL container
===========================
* The user needs to create two folders, for example

::

  mkdir $HOME/postgresql
  mkdir $HOME/postgresql/data
  mkdir $HOME/postgresql/temp

* The user needs to map previous two folders when running the container,

  * "$HOME/postgres/data" folder is used for saving the databases
  * "$HOME/postgres/temp" folder is used by the postgres server for temporary files


*	Use the following command to start up the server

::

  SINGULARITYENV_POSTGRES_PASSWORD=password singularity instance start  -B $HOME//postgresql/data:/var/lib/postgresql/data -B $HOME/postgresql/temp:/var/run/postgresql/ postgres_container.sif  postgres  -p 5432

* Please note that:

  * password (something else may be used), is the database user password for the database user "postgres".The user will need both of them to configure the database server connection.
  * The default port number for the database server is 5432, it is possible to change it by replacing 5432 with the new port.
  * The instance name is "postgres"