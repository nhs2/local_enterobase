Developing Local EnteroBase
===========================

Singularity images creation
---------------------------

Local EnteroBase is hosted in `Bitbucket repository <https://bitbucket.org/enterobase/local_enterobase/src/master/>`_

The following figure illustrates the Local EnteroBase folder structure, all the images definition files are saved in the Singularity_Images sub-folder

.. figure:: ../images/sing_folders_3.png
   :width: 300
   :alt: Local EnteroBase folder Structure

   **Local EnteroBase folder structure**


.. toctree::
   :maxdepth: 2

   nginx_image
   postgres_image
   gunicorn
   EToKi_image






