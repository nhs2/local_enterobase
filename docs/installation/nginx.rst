NGINX Web Server
^^^^^^^^^^^^^^^^

* Pull the nginx singularity image “nginx_container.sif” from the singularity cloud library using the following command

  ::

    singularity pull --arch amd64  nginx_container.sif library://local_enterobase/default/nginx_local_enterobase:0.1

* Please run the following commands to create a folder and copy nginx configuration file (nginx_local_enterobase.conf) and certs folder to the created folder.

  ::

    mkdir $HOME/nginx_folder
    singularity run -B $HOME/nginx_folder:/home/nginx_user   --app prep_nginx nginx_container.sif

* Then you should alert nginx_local_enterobase.conf to set the domain  name or IP address in three different places, by replacing (replaced_by_your_server_uri) string with your actual server uri, e.g. myserver.com.
* In addition, you should provide a valid ssl certificate which should be saved inside the certs folder inside the user's home folder
* In case of your server does not have a valid ssl certificate, you can temporary create a self-signed certificate using the following link.

  * https://linuxize.com/post/creating-a-self-signed-ssl-certificate/
* Alternatively, you can use the keys inside certs folder in the user home folder (no modification is needed)
* This should be temporary, as it is important to get a valid ssl certificate from a trusted Certificate Authority.

* To test your configuration, you should run this command:

  ::

    sudo singularity run -B $HOME/nginx_folder:/home/nginx_user   --writable-tmpfs  --app test_nginx  nginx_container.sif

* You need to correct any issue which you may get from the previous command, and run the test again.

* You need to run the server using the following command

  ::

    sudo singularity instance start -B  $HOME/nginx_folder:/home/nginx_user   --writable-tmpfs  nginx_container.sif nginx_sing

