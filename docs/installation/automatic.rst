Automatic installation
----------------------

The Installtion script "local_enterobase_installer.sh" is saved in the "installer" sub-folder.

The script will automatically download, configure and run the system singularity images (i.e. Nginx, PostgreSql and Gunicorn and local EnteroBase) and their dependencies packages.
During the installation the user will be asked to:

- Provide his server domain name or IP address
- Set a password for local EnteroBase.

Then system components will be configured using the default parameters and should run without any issue.

The following figure shows the script workflow.

.. figure:: ../images/automatic_installation_script.png
   :width: 400
   :alt: Automatic installation workflow


