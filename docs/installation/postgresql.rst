PostgreSQL Database server
--------------------------

* Please pull the postgres singularity image file “postgres_container.sif” from the singularity  cloud library using the following command:

  ::

    singularity pull --arch amd64 postgres_container.sif library://local_enterobase/default/postgres_local_enterobase:0.1

* You need to create a folder which will be used to save the databases (for example: $HOME/postgresql/data) and create another folder (for example $HOME/postgresqltemp) for the database server temporary use, e.g. postgres_temp, these two folders will be bonded with related folders inside the postgres container at run time

  ::

    mkdir $HOME/postgresql
    mkdir $HOME/postgresql/data
    mkdir $HOME/postgresql/temp

* Then you can run the following command to start up the server

  ::

    SINGULARITYENV_POSTGRES_PASSWORD=local_password singularity instance start -B $HOME/postgresql/data:/var/lib/postgresql/data  -B $HOME/postgresql/temp:/var/run/postgresql/   postgres_container.sif   postgres  -p 5432

* Please note that:

  * local_password (you may use something else), is the database user password for the database user postgres, you will need both of them to configure your local instance
  * The default port number for the database server is 5432, you may change that by replacing 5432 with the new port
