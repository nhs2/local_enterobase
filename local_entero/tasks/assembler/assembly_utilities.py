import local_entero.databases.databases_utilities as d_util

from local_entero import warwick_enterobase, db
from config import app_config


print (app_config.DATABASE, app_config.AUTHORIZE_URL)



def send_meta_data_to_warwick_enterobase(st_id):
    print ("sid: ", st_id)
    strain_metdata=d_util.get_raw_strain(st_id, db)
    c_url = r"/store_assembled_strain_metadata/?data=%s, database=%s"%(strain_metdata.get("data"), strain_metdata.get("database"))
    try:
        resp = warwick_enterobase.get(c_url)
        print (resp)
        print (resp.__dict__)
        print (resp.__dict__.keys())
        return (resp.data)
    except:
        print("ERROR, please try later")
        return "Error"# json.dumps(create_results_message(True, message="database is not valid"))

def upload_assembly_file_to_warwick_enterobase(st_id):
    pass


def update_local_instance_from_warwick_enterobase(st_id):
    pass