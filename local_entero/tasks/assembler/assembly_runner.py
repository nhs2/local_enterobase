"""
PURPOSE     : there are two main methods check jobs status and send an assembly
job if there is available slot,  run the assembly jobs and save the results.

| SPECIAL NOTES:  It is not fully implmeneted yet it should be modified to:
| * integrate Singularity EToKi version.
| * test run the assembler and save the results to the database
| * send the strains metadata, along with the assembly file, which are assembled successful to EnteroBase.

| Revision History:
| Name:version 0.1
| Date: 02/2020
| Description: deployed

"""

import os
import sys
import uuid
#from celery.task.control import revoke

from celery.signals import worker_process_init, worker_process_shutdown

main_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
sys.path.append(os.path.abspath(main_dir))
mm=os.path.join(main_dir, '..')
sys.path.append(os.path.abspath(os.path.join(main_dir, '..')))
sys.path.append(os.path.abspath(os.path.join(mm, '..')))

from local_entero import create_app, warwick_enterobase

from config import configLooader


# This should be modified to work with the production server
app_config=configLooader["development"]
local_app, celery=create_app("development")


from subprocess import Popen
from flask import current_app
import subprocess
from shutil import copyfile
import local_entero.databases.databases_utilities as d_util

from celery.result import AsyncResult
from flask_sqlalchemy import SQLAlchemy
db_conn = None

@worker_process_init.connect
def init_worker(**kwargs):
    global db_conn
    print('Initializing database connection for worker.')
    db_conn = create_database()


@worker_process_shutdown.connect
def shutdown_worker(**kwargs):
    global db_conn
    if db_conn:
        print('Closing database connectionn for worker.')
        db_conn.close()



@celery.task(bind=True)
def run_assembly(assembly_file=None):
    print ("It is working ....")
    #_run_assembly()

@celery.task(bind=True)
def _run_assembly(self, job_id, read_files):
    #toDo copy read files to working folder
    """
    run an assembly job and update the job status, and output when it finished.

    :param self:
    :param job_id:
    :param read_files:

    """

    # todo: check the read files exist before running the assembly job
    # if the assembly files is exist, then, it should be renamed as (old_assembly_file) and run the assembly
    print (_run_assembly.request.id)
    returned_results={}
    updated_fields = {}
    print ("running new assembly process ........................")
    if job_id==0:
        job_folder_ = str(uuid.uuid4().fields[-1])[:7]
    else:
        job_folder_=str(job_id)
    job_folder=os.path.join(local_app.config.get('WORKING_FOLDER'), job_folder_)
    if not os.path.exists(job_folder):
        os.makedirs(job_folder)
    updated_fields['status'] = "Running"
    updated_fields['process_id'] = _run_assembly.request.id
    updated_fields['job_folder']=job_folder
    update_database(job_id, updated_fields)
    current_app.logger.info("Job working folder: %s"%job_folder)
    os.chdir(job_folder)
    assembly_file=os.path.join(job_folder,"_assembly_results.result.fasta")
    current_app.logger.info("Assembly file: %s"%assembly_file)


    cmd = [local_app.config.get('ETOKI_PYTHON') , local_app.config.get('ASSEMBLY_COMMAND'),
            "EBAssembly", "-f", read_files,
            "-o",
            job_folder]
    print (cmd)
    current_app.logger.info("Running assembly job")
    process = Popen(cmd ,stdout=subprocess.PIPE, stderr=subprocess.PIPE,cwd=job_folder, encoding='utf8', text=True)
    process.wait()
    stdout, stderr = process.communicate()
    current_app.logger.info("process output%s"%stdout)
    current_app.logger.info("Errors: %s", stderr)
    # check if the output file exist
    if os.path.isfile(assembly_file):
        dst=os.path.join(app_config.OUTPUT_FOLDER,job_folder_+'.fasta')
        copyfile(assembly_file, dst)
        returned_results['output_location']=dst
        returned_results['status'] = "Completed"
        jobs_results={'stdout': str(stdout), 'stderr': stderr}
        st_id=update_database(job_id, returned_results, jobs_results)
        print ("Going to send the strain data to warwick enterobase")
        strain_metdata = d_util.get_raw_strain(st_id, db_conn)

        c_url = r"/store_assembled_strain_metadata/?data=%s, database=%s" % (
        strain_metdata.get("data"), strain_metdata.get("database"))
        print("address", c_url)
        local_app.app_context().push()

        with local_app.app_context():

            try:
                print("I am the connector ..")
                resp = warwick_enterobase.get(c_url)
                print(resp)
                return (resp.data)
            except Exception as e:
                print("ERROR, please try later", e)
                return "Error"  # json.dumps(create_results_message(True, message="database is not valid"))

    else:
        # the output is not exist so the job is failed
        returned_results['status'] = "Failed"
        jobs_results={'stdout': str(stdout), 'stderr': stderr}
        update_database(job_id, returned_results, jobs_results)




def abosrt_task(task_id):
    from celery.contrib.abortable import AbortableAsyncResult
    abortable_task = AbortableAsyncResult(task_id)
    abortable_task.abort()

@celery.task(name ="check_jobs_queue")
def check_jobs_queue():
    """
    this method runs every 1 minute
    it will check the jobs
    and if the number of running jobs is less than  the allowed running jobs
     it will run the diffrence number between of them
     it sorts the jobs according to their priorities
     e.g. job with priority -8 will run before job with priority with priortity -6

     """
    #db = create_database()
    try:
        #db.engine.dispose()
        print('Hi! from check_jobs_queue...')

        jobs=d_util.get_all_jobs(db_conn)
        running_jobs=[]
        waiting_jobs=[]
        for job in jobs:
            print ("Job id: ", job.id)
            if (job.status == 'Running' or job.status == 'Processing'):
                print ("Process id: ",job.process_id)
                try:
                    res = AsyncResult(id=job.process_id)
                    print ("RESULT STATUS: ", res.status, job.id)
                    returned_results = {}
                    if res.status=='SUCCESS' or res.status=='FAILURE':
                        returned_results['status']=res.status
                        returned_results['results']=str(res.result)
                        update_database(job.id, returned_results)
                    elif res.status== "PENDING":
                        print ("I am in PENDING")
                        running_jobs.append(job)
                    print (res.status, res.result, job.id)
                    print ("The status", type(res.status), res.result, job.id)
                    continue
                except Exception as e:
                    print ("Error message: %s"%e)

                running_jobs.append(job)
            elif job.status=='Received' or not job.status:
                waiting_jobs.append(job)
        print ("Running taks: %s"%len(running_jobs))
        if len(running_jobs)<app_config.ALLOWED_PROCESS:
            c=app_config.ALLOWED_PROCESS-len(running_jobs)
            print ("av slots: %s"%c)
            if len(waiting_jobs)==0:
                return
            sorted_waiting_jobs = sorted(waiting_jobs, key=lambda x: x.priority)#, reverse=True)
            for i in range(0,c):
                job_=sorted_waiting_jobs[i]
                _run_assembly.delay(job_.id, job_.read_files)
    except Exception as e:
        print("Unexpected error:", sys.exc_info()[0])
        print (e)

     #   print ("Error: %s"%e.message)


#results=_run_assembly.delay()
#print job.as_dict()
#results.id

'''
status
Running
Completed 
Queued 
Failed
Received
'''

def create_database():
    db_conn=SQLAlchemy()
    app_config.SQLALCHEMY_BINDS = dict(jobs_db=app_config.SQLALCHEMY_DATABASE_URI)
    #db_conn.init_app(local_app)
    return db_conn

def update_database(job_id, modified_data, jobs_results=None):
    """
    update the job

    :param job_id:
    :param modified_data: data to be saved inside the database

    """
    print ("Updating: ", modified_data)
    #db=create_database()
    #db_=create_database()
    st_id=d_util.update_job(job_id, modified_data, db_conn, jobs_results)
    mod_strain_data={}
    if modified_data.get('status'):
        if modified_data.get('status')=="Running":
            mod_strain_data['status'] = 'Processing'
        else:
            mod_strain_data['status']=modified_data['status']
        mod_strain_data['aasembly_file']=modified_data.get('output_location')
    d_util.update_strain_data(st_id, mod_strain_data, db_conn)
    return st_id

def upload_strain_to_Warwick_EnteroBase():
    """
    if the assembly process succssed, then it will  upload the assembly file along with the
    strain metadata to Warwick EnteroBase

    """
    pass




def send_meta_data_to_warwick_enterobase(st_id):
    print ("Hi from the send method =====>>>")
    #print(warwick_enterobase.__dict__)

    print ("sid_____: ", st_id)
    strain_metdata=d_util.get_raw_strain(st_id, db_conn)
    print ("I am wondering if it is working ...")
    c_url = r"/store_assembled_strain_metadata/?data=%s, database=%s"%(strain_metdata.get("data"), strain_metdata.get("database"))
    #print(warwick_enterobase.__dict__)
    print (c_url)
    with local_app.app_context():
        try:
            print ("I am the connector ..")
            resp = warwick_enterobase.get(c_url)
            print (resp)
            #print (resp.__dict__)
            #print (resp.__dict__.keys())
            return (resp.data)
        except Exception as e:
            print("ERROR, please try later", e)
            return "Error"# json.dumps(create_results_message(True, message="database is not valid"))