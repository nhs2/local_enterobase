"""
Purpose     : Strains metdata form

| Revision History:
| Name:version 0.1
| Date: 02/2020
| Description: deployed

"""

from flask_wtf import FlaskForm as Form
from config import app_config as config
from local_entero import local_app
from wtforms import StringField, IntegerField, FormField,SelectField, FieldList
from wtforms.validators import DataRequired
from flask_wtf.file import FileField, FileRequired,FileAllowed
#from flask_uploads import UploadSet, ARCHIVES
from datetime import date
from wtforms.fields.html5 import DateField


allowed_Sequencing_Library=config.ALLOWED_Sequencing_Library
allowed_Sequencing_Platform=config.ALLOWED_Sequencing_Platform
#ARCHIVES = UploadSet('ARCHIVES', ARCHIVES)



class Data_file_Form(Form):
    DataFile = FileField(label="Data file* ", validators=[FileRequired(message="data file is required"),                                                             ],
                          render_kw={"accept*": ".*", "placeholder": "Drag and drop data file"})


class Strain_basic_form(Form):
    strain = StringField(label="Strain Name*", validators=[DataRequired()],
                         render_kw={"placeholder": "Strain name, required"})

    contact = StringField(label="Lab Contact*", validators=[DataRequired()],
                          render_kw={"placeholder": "Lab contact, required"})
    collectiondate = DateField(label="Collection Date*", validators=[DataRequired()], render_kw={"class": 'datepicker'})


class Read_files_Form(Form):
    Readfile1 = FileField(label="Read File 1* ", validators=[
                                                             FileAllowed(['gz'], message='gz archive files only!')],
                          render_kw={"accept": ".gz", "placeholder": "Drag and drop gz file"})



    Readfile2 = FileField(label="Read File 2* ", validators=[
                                                            FileAllowed(['gz'], message='gz archive files only!')],
                          render_kw={"accept": ".gz", "placeholder": "Drag and drop gz file"})


class Strain_metedata_Edit_Form(Strain_basic_form):
  pass

class Strain_metedata_Form(Strain_basic_form, Read_files_Form):
  pass


class StrainMetadataGridTableForm(Form):
    """A form for one or more Strain_metedata_Form"""
    pass


def get_fields_group(fields, coulumn_no, data):
    class Meta:
        csrf = False

    group_name=fields[0].get('group_name')


    rej_grp=["Project","Sample", "Accession", "Collection Date"]

    grp_cls = type(group_name, (Form, ), {'__doc__': 'class created for goup'})
    grp_cls.Meta=Meta

    if group_name in rej_grp:
        return None, None

    if group_name=="Location":
        pass
    fields_group={}
    for field in fields:
        form_field=get_field_details(field, data)
        if form_field:
            coulumn_no+=1
            setattr(grp_cls, field.get('name'), form_field)

    return group_name, grp_cls

def get_field_details(field, data):
    ignored_fields=["Sequencing Platform","Sequencing Library", "Accession", "Experiment", "Bases","Region", "District","Post Code","Collection Time","Collection Year","Collection Month","Collection Day","Insert Size", "Barcode","Date Entered","Release Date","Uberstrain","Accession No.", "Name"]

    if not field.get("label") in ignored_fields:
        form_field = getField(field, data)
        return form_field

def set_field_value(field, row_data):
    label=field.label.text.strip()
    from werkzeug.datastructures import FileStorage
    value=None
    conv={'':''}
    if not label:
        return value
    if label in row_data:
        value =row_data.get(label)
    else:
        if label=="Strain Name*":
            value=row_data.get('Name')
        elif label=='Read File 1*' and row_data.get("Read Files"):
            print (type(field), "file_type")
            if len(row_data.get("Read Files").split(','))==2:
                value__=row_data.get("Read Files").split(',')[0]
                print (field.data,"data")
                field.data=FileStorage(filename= value__, name=value__)

                print ("Value 1", field.data.filename)

        elif label=='Read File 2*' and row_data.get("Read Files"):
            if len(row_data.get("Read Files").split(','))==2:
                value__=row_data.get("Read Files").split(',')[1]
                field.data = FileStorage(filename=value__, name=value__)
                print(field.data, "data222")
                print("Value 2", field.data.filename)

        elif label=='Lab Contact*':
            value = row_data.get("Lab Contact")
        elif label=='Serological Group':
            value = row_data.get("Serotype")
        elif label=='Source Niche':
            value = row_data.get('Source')
        elif label=='Collection Date*':
            day=row_data.get('Collection Day')
            if not day:
                day=1
            month=row_data.get('Collection Month')
            if not month:
                month=1
            year=row_data.get('Collection Year')
            if year:
                value=date(year=int(year), month=month, day=day)
    if value and label!='Read File 2*'and label!='Read File 1*':
            field.data=value


def _get_fields(field, row_data):
    if field.type == "FieldList":
        for field_ in field:
            if field_.type == "CSRFTokenField":
                continue
            set_field_value(field_, row_data)

    else:
        set_field_value(field, row_data)

def create_strain_metedata_forms_table(strain_metadata,md_json):
    create_strain_metedata_form(strain_metadata)
    strain_rows = FieldList(FormField(Strain_metedata_Form), min_entries=len(md_json))
    setattr(StrainMetadataGridTableForm, "strain_rows", strain_rows)
    strainMetadataGridTableForm = StrainMetadataGridTableForm()
    for i in range (0, len(md_json)):
        row=strainMetadataGridTableForm.strain_rows[i]
        row_data=md_json[i]
        print ("row data",row_data )
        print (row_data.get('Read Files'))
        fields=[]
        type_f=[]
        for field in row:
            if field.type not in type_f:
                type_f.append(field.type)
            if field.type == "CSRFTokenField":
                continue
            elif field.type == "FieldList":
                for field_ in field:
                    for field__ in field_:
                        if field__.type == "CSRFTokenField":
                            continue
                        _get_fields(field__, row_data)
            else:
                fields.append(field)
                set_field_value(field, row_data)

        for key, value in row_data.items():
            pass
            try:
                setattr(fields, key, value)
            except:
                pass
    return strainMetadataGridTableForm

def create_strain_metedata_form(metedata, data=None):
    coulumn_no=4
    for field in metedata:
        if isinstance(field, list):
            group_name,fields=get_fields_group(field, coulumn_no, data)
            if group_name:
                coulumn_no += 1
                grp_rows = FieldList(FormField(fields), min_entries=1)
                # ListWidget
                setattr(Strain_metedata_Form, group_name, grp_rows)
        else:
            form_field=get_field_details(field, data)
            if form_field:
                setattr(Strain_metedata_Form, field.get('name'), form_field)
                coulumn_no += 1

    strain_metadata_from = Strain_metedata_Form()
    return coulumn_no, strain_metadata_from

def _check_options(vals_, label):
    allowed_set=None
    if label=="Sequencing Platform":
        allowed_set=allowed_Sequencing_Platform
    elif label=="Sequencing Library":
        allowed_set=allowed_Sequencing_Library
    vals=[]
    for val in vals_:
        if allowed_set:
            if val.lower() in allowed_set:
                vals.append(val)
        else:
            vals.append(val)

    if len(vals)>1:
        vals.insert(0,' ')
    return vals

def getField(field, data):
    if field.get('label')=='Continent':
        vals=local_app.config.get('CONTINENTS')
        choices = [(val, val) for val in vals]
        if field.get("required"):
            return SelectField(field.get('label'), choices=choices, validators=[DataRequired()])
        return SelectField(field.get('label'), choices=choices)

    elif field.get('label')=='Country':
        vals = local_app.config.get('COUNTRIES')
        choices = [(val, val) for val in vals]
        if field.get("required"):
            return SelectField(field.get('label'), choices=choices, validators=[DataRequired()])
        return SelectField(field.get('label'), choices=choices)

    if field.get("datatype")== "text":
        if field.get("required"):
            return StringField(field.get('label'),validators=[DataRequired()])
        return StringField(field.get('label'))
    elif field.get("datatype")== "integer":
        if field.get("required"):
            return IntegerField(field.get('label'),validators=[DataRequired()])
        return IntegerField(field.get('label'))

    elif field.get("datatype")== "combo":
        check_fields=['Sequencing Platform', 'Sequencing Library']
        if field.get('vals'):
            vals=field.get('vals').split('|')
            #if field.get('label')in check_fields:
            vals=_check_options(vals, field.get('label'))

            choices = [(val, val) for val in vals]
            if field.get("required"):
                return SelectField(field.get('label'),choices=choices,validators=[DataRequired()])
            return SelectField(field.get('label'),choices=choices)
    elif field.get("datatype")== "custom":
        if field.get("required"):
            return StringField(field.get('label'),validators=[DataRequired()])
        return StringField(field.get('label'))

