"""
Purpose     : Create Oauth server

| Revision History:
| Name:version 0.1
| Date: 02/2020
| Description: deployed

"""

from flask import url_for

from flask_oauthlib.client import OAuth, OAuthRemoteApp

class warwick_enterobase_remote (OAuthRemoteApp):
    def __init__(self, bas,name, **kwargs):
        super(warwick_enterobase_remote, self).__init__(bas,name,**kwargs)


    def _tokengetter(self):
        return "TTTTTTTTT"

class Warwick_EnteroBase(OAuth):
    providers = None
    def __init__(self, cur_app):
        super(Warwick_EnteroBase, self).__init__(cur_app)
        self.provider_name = 'warwick_enterobase'

    def set_consumer (self,name, **kwargs):
        pass

    def remote_app(self, name, register=True, **kwargs):
        """Registers a new remote application.

        :param name: the name of the remote application
        :param register: whether the remote app will be registered

        Find more parameters from :class:`OAuthRemoteApp`.
        """
        remote = warwick_enterobase_remote(self, name, **kwargs)
        if register:
            assert name not in self.remote_apps
            self.remote_apps[name] = remote
        return remote


def create_warwick_connector(config_, oauth):
    config=config_.__dict__
    warwick_enterobase = oauth.remote_app(
            'warwick_enterobase',
            consumer_key=config.get('CLIENT_ID'),
            consumer_secret=config.get('CLIENT_SECRET'),
            request_token_params={
                'scope': 'Username, Email, Submitting Assemblies'
            },
            base_url=config.get('BASE_URL'),
            request_token_url=None,
            access_token_method='POST',
            access_token_url=config.get('ACCESS_TOKEN_URL'),
            authorize_url=config.get('AUTHORIZE_URL'),

        )
    return warwick_enterobase





    def authorize(self):
        pass

    def callback(self):
        pass

    def get_callback_url(self):
        return url_for ('auth_callback', provider=self.provider_name,_external=True)

    @classmethod
    def get_provider(self, provider_name):
        if self.providers is None:
            self.providers = {}
            for provider_class in self.__subclasses__():
                provider = provider_class()
                self.providers[provider.provider_name] = provider
        return self.providers[provider_name]


class EnteroBase(OAuth):
    def __init__(self, current_app):
        super(EnteroBase, self).__init__('warwick_enterobase')
        self.service = OAuth2Service(
            name='warwick_enterobase',
            client_id=self.consumer_id,
            client_secret=self.consumer_secret,
            authorize_url='https://graph.facebook.com/oauth/authorize',
            access_token_url='https://graph.facebook.com/oauth/access_token',
            base_url='https://graph.facebook.com/'
        )