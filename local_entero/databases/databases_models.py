"""
Purpose     : System database table models

| Revision History:
| Name:version 0.1
| Date: 02/2020
| Description: deployed

"""

import sys
import os
main_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
+ '/local_entero/')
sys.path.append(main_dir)

#from local_entero  import db
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy  import MetaData
from flask_sqlalchemy import SQLAlchemy

from flask_login import UserMixin

metadata = MetaData()

Base = declarative_base(metadata=metadata)
db = SQLAlchemy()
class Jobs (db.Model):#, Base):
    """
    the jobs which will be sent to the assembler.

    """
    #__bind_key__ = "local_db"
    __tablename__ = 'jobs'
    id = db.Column( db.Integer, primary_key=True)
    strain_data_id=db.Column(db.Integer, db.ForeignKey('strains_data.id',ondelete="CASCADE"))
    process_id=db.Column( db.String)
    priority=db.Column(db.Integer)
    read_files=db.Column(db.String)
    job_folder=db.Column(db.String)
    user_id = db.Column(db.Integer)
    status = db.Column(db.String)
    results= db.Column(db.String)
    pipeline = db.Column(db.String)
    date_received= db.Column(db.DateTime)
    database = db.Column(db.String)
    accession = db.Column(db.String)
    output_location = db.Column(db.String)
    version = db.Column(db.String)
    last_updated= db.Column(db.DateTime)
    job_data = db.Column(JSONB)

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        return '<Jobs %r>' % self.process_id

class Users(UserMixin, db.Model):
    """
    user basic information,
    the actual log in will be from Warwick EnteroBase.

    """
    #__bind_key__ = "local_db"
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    enterobase_id = db.Column(db.Integer, nullable=False, unique=True)
    username = db.Column(db.String(64), nullable=False)
    email = db.Column(db.String(64), nullable=True)
    token=db.Column(db.String(264), nullable=True)
    is_admin = db.Column(db.Boolean, nullable=True, default=False)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        return '<User %r>' % self.enterobase_nickname


class UsersAssemblyReadsFiles(db.Model):
    """
    store read files names and linked to strain_data table.

    """
    #__bind_key__ = "local_db"
    __tablename__ = 'readfiles'
    id = db.Column(db.Integer, primary_key=True)
    strain_data_id=db.Column(db.Integer, db.ForeignKey('strains_data.id',ondelete="CASCADE"))
    file_name = db.Column(db.String(100))
    file_location = db.Column( db.String(200))
    database = db.Column(db.String(20))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    status = db.Column( db.String(60))
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Strain_Data(db.Model):
    """
    store strain metadata in addition to read files names.

    """
    #__bind_key__ = "local_db"
    __tablename__ = 'strains_data'
    id = db.Column(db.Integer, primary_key=True)
    strain = db.Column(db.String(64), nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    database = db.Column(db.String(20))
    date_uploaded = db.Column(db.DateTime, default=db.func.now())
    last_updated = db.Column(db.DateTime, default=db.func.now())
    release_period=db.Column(db.Integer)
    status = db.Column( db.String(60))
    type = db.Column( db.String(20))
    aasembly_file = db.Column(db.String)
    data = db.Column(JSONB)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

def create_tables():
    #try:
    db.create_all()
    #except Exception as msg:
    #    print ("Error")
    #    new_msg = 'Blabla, %s.' % msg
    #    print(Exception, Exception(new_msg), sys.exc_info()[2])

