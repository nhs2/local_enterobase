function set_background_func()  {
    var main_na = document.getElementById('name_base');
    var label = document.getElementById('main_label');
 //check if the background image in the confguration file is in the server
 //if not it will load the default image
        $.ajax({
              type: "HEAD",
              url : "/get_icon",
              success: function(message,text,response){
                 $("#name_base" ).css('background-image', ['url(/get_icon)','url(static/img/salmonella.jpg)']);
                 $("#name_base" ).css('background-size',['contain','contain']);
                 $("#name_base" ).css('background-repeat',['no-repeat' ,'repeat']);
                 $("#name_base" ).css('background-position',['right','left']);
                 $("#name_base" ).css('background-color','tan');
              },
           error : function(request,error)
           {
           console.log("error found: "+error+", request: "+request);
              $( "#name_base" ).css('background-image','url(static/img/salmonella.jpg)');
           }
    });

    label.style.fontSize = "45px";
    label.style.fontWeight='bold';
    label.style.color = "black";

     $.each($('#dbase_page_nav').find('li'), function() {
        $(this).toggleClass('active',
                    '/' + $(this).find('a').attr('href') == window.location.pathname);
     });

         $(".current-menu-item").addClass("active");

         $.each($('#dbase_page_nav').find('li'), function() {
        $(this).toggleClass('active',
            window.location.pathname.indexOf($(this).find('a').attr('href')) > -1);
    });

}
