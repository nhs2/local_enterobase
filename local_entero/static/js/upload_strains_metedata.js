 var dataView;
  var slickgrid;
  var gridMenu;
var reco_read_files_extension={"_R1.fastq.gz":"_R2.fastq.gz","_R1_001.fastq.gz":"_R2_001.fastq.gz","_R1.fq.gz":"_R2.fq.gz","_R1_001.fq.gz":"_R2_001.fq.gz"};
 var group_reads=[];
var session_read_files=[];


function read_files_uploader_2()  {
    $("#readfiles").fileinput({
        uploadUrl: "/upload_files_2",
        uploadAsync: true,
        showPreview: false,
        showUpload :false,
        allowedFileExtensions: ['gz'],
        maxFileCount: 5,
        elErrorContainer: '#elErrorContainer'
    }).on('filebatchpreupload', function(event, data, id, index) {
        $('#sucErrorContainer').html('<h4>Upload Status</h4><ul></ul>').hide();
    }).on('fileuploaded', function(event, data, id, index) {
        var fname = data.files[index].name,
        out = '<li>' + 'Uploaded file # ' + (index + 1) + ' - '  +  fname + ' successfully.' + '</li>';
        $('#sucErrorContainer ul').append(out);
        $('#sucErrorContainer').fadeIn('slow');
    });
}

function read_files_uploader () {
    $("#readfiles").fileinput({
        uploadUrl: "/upload_files_2",
        uploadAsync: false,
        showPreview: false,
        showUpload :false,
        allowedFileExtensions: ['gz'],
        maxFileCount: 5,
        elErrorContainer: '#elErrorContainer'
    }).on('filebatchpreupload', function(event, data, id, index) {
        $('#sucErrorContainer').html('<h4>Upload Status</h4><ul></ul>').hide();
    }).on('filebatchuploadsuccess', function(event, data) {
        var out = '';
        $.each(data.files, function(key, file) {
            var fname = file.name;
            out = '<li>' + 'Uploaded file # ' + (key + 1) + ' - '  +  fname + ' successfully.' + '</li>';
             $('#sucErrorContainer').append(out);
            $('#sucErrorContainer').fadeIn('slow');
        });

    });
}




function config_open_import_file_dialoug()
{
//dialoug_title
  $("#ex_im_title").text("Import");
  $("#import_btn").show();
  $("#fileimporter").show();
 $("#save_btn").hide();
 $("#efname").hide();
  $("#efname_lab").hide();
 $("#dialoug_explain").text("Load metadata to a text file");
 $('#export_import_dialoug').modal('show');



}

function config_open_export_file_dialoug()
{
//data-toggle="modal" data-target="#export_dialoug"

 $("#ex_im_title").text("Export");

  $("#import_btn").hide();
  $("#fileimporter").hide();
 $("#save_btn").show();
 $("#efname").show();
  $("#efname_lab").show();

 $("#dialoug_explain").text("Save the metadata to a text file");
 $('#export_import_dialoug').modal('show');


}

function add_files()
 {
  /*
     add paired read files to the strain metadata table
     it will insert rows which contains the reads_files
 */
if (group_reads.length>0 || session_read_files.length>0)
{
    for (i in group_reads)
    {
    var name;
    try
    {
     this.name=group_reads[i].split(',')[0].split('_R')[0];
    }
    catch(error){

    console.log(error);}
         console.log("Name: "+this.name);

    item = {
          "id": dataView.getLength()+1+i,
          "read_files": group_reads[i],
          "strain": this.name
        };
     dataView.insertItem(dataView.getLength(), item);
     $("#submit_metadata").prop("disabled", true);
     slickgrid.invalidate();
     slickgrid.render();
     }
     dataView.fastSort('read_files', true)

      group_reads.length=0;
      $('#add_file_dialoug').modal('hide');
      }
 }

function check_selected_read_files(){
var input = document.getElementById('readfiles');
//empty list for now...
while (group_reads.length) {
    group_reads.pop();
  }
var read_files=[];
var not_valid={};
var not_valid_files=0;

for (var x = 0; x < input.files.length; x++) {
	isvalid=check_read_file(input.files[x].name, input.files[x].size);

 	if (isvalid==true)
	   read_files.push(input.files[x].name);
	else
	{
	   input.files[x].value='';
	   not_valid[input.files[x].name]=isvalid;
	   not_valid_files=++not_valid_files;
    }

}
    console.log(read_files.length+" files are selected")
    if(not_valid_files>0)
      console.log(not_valid_files +" files are not valid\n"+not_valid);
     read_files_seprator(read_files);
     addTable(group_reads);
     session_read_files=session_read_files.concat(read_files);
     console.log("1:::::::"+ session_read_files[0]);
 }

function addTable(r_files) {
 try
  {
  document.getElementById("re_table").remove();
  }
  catch(error){}

  var tableDiv = document.getElementById("read_files_table");

  var table = document.createElement('TABLE');
      table.setAttribute('id', "re_table");

  table.border = '1';

  var tableBody = document.createElement('TBODY');
  table.appendChild(tableBody);

  for (var i = 0; i < r_files.length; i++) {
    var tr = document.createElement('TR');
    tableBody.appendChild(tr);

    var td = document.createElement('TD');
    td.width = '75';

    td.appendChild(document.createTextNode(r_files[i]));
    tr.appendChild(td);
  }
  tableDiv.appendChild(table);
}

function check_read_file(filename,file_size){
//check files to besure they are not added before
// and also for their sizes
    //check file name for sepcial characters
   restricted_chrs=['-','!','@','#','$','%','^','&','*','(',')','+','=','[',']','{','}',';','\'',':','"','|',',','<','>','/','?'];
      for (cr in restricted_chrs)
       {
       if (filename.indexOf(restricted_chrs[cr]) > -1)
           {
              header = "Upload read file error";
              body="The file name should not contain special characters like [-!@#$%^&*()+=[]{};':\"|,<>/?]"
              //displayMessage(header, body);
              source.value='';
              return body;
           }
       }

//check if file has been used before
    if (user_files_list.indexOf(filename)>-1)
     {
          header = "Upload read file error";
          body=filename+" is associated with other strain";
          //displayMessage(header, body);
          return body;
      }


   //check if file has been used before
    if (session_read_files.indexOf(filename)>-1)
     {
          header = "Upload read file error";
          body=filename+" is selected before";
          //displayMessage(header, body);
          return body;
      }

//check file name (extension should be gz)
      if (!filename.toLowerCase().endsWith(".gz"))
      {
          header = "Upload read file error";
          body=filename+" needs to be gzip file";
          //displayMessage(header, body);
          return body;
      }
//check file size (should be > 2MB)
    if (file_size < 2097152)
    {
          header = "Upload read file error";
          body=filename+" ("+file_size+" byte) size is less than 2MB";
          //displayMessage(header, body);
          return body;
    }

    return true;

}

function get_file_extenison(read_files){
//check the read files extensions by compating it with the recognized extension supported by the script
    for (var ext_1 in reco_read_files_extension) {
    if (reco_read_files_extension.hasOwnProperty(ext_1)) {
    if (read_files[0]  == ""|| read_files[0] ==null ||read_files[0]  == undefined  )
        continue
       if (read_files[0].endsWith(ext_1) || read_files[0].endsWith(reco_read_files_extension[ext_1]))
                            return [ext_1, reco_read_files_extension[ext_1]];
    }
            }
            return []

    }

function read_files_seprator(read_files){
    //example of read file Data\Intensities\BaseCalls\SampleName_S1_L001_R1_001.fastq.gz
    // it should have two file, with R2 as above
    //Data\Intensities\BaseCalls\SampleName_S1_L001_R2_001.fastq.gz
    //please note some users delete _001 by the end so we need to account for it
    //e.g. Data\Intensities\BaseCalls\SampleName_S1_L001_R1.fastq.gz
    //source:
    //https://support.illumina.com/help/BaseSpace_OLH_009008/Content/Source/Informatics/BS/NamingConvention_FASTQ-files-swBS.htm
    //_R1.fastq.gz
    //_R2.fastq.gz

    ext=get_file_extenison(read_files);
    if (ext.length!=2)
       return groups;
    r1_ext=ext[0];
    r2_ext=ext[1];
    console.log(r1_ext+", "+ r2_ext);
    var len=read_files.length;
    var already_added=[];
    var groups=[];
    //r1_ext="_R1.fastq.gz";
    //r2_ext="_R2.fastq.gz";

    for (var i = 0; i < len; i++) {
    if (already_added.indexOf(read_files[i])>=0)
        continue;
         var other_read;
       if (read_files[i].endsWith(r1_ext)){
           other_read=read_files[i].replace(r1_ext,r2_ext)
          }
            else if (read_files[i].endsWith(r2_ext)){
              other_read=read_files[i].replace(r2_ext,r1_ext)
                          }

      for (var j = 0; j < len; j++) {
          if (already_added.indexOf(read_files[j])>=0|| i==j)
             continue;

             if (read_files[j]==other_read){
             already_added.push(read_files[i]);
             already_added.push(read_files[j]);
             group_reads.push(read_files[i]+","+read_files[j])
             }
      }
    }
 }

var options = {
    enableCellNavigation: true,
    enableColumnReorder: false,
    multiColumnSort: true,
    explicitInitialization: true,
    //showHeaderRow: true,
    headerRowHeight: 30,
    //autoHeight: true,
    preHeaderPanelHeight: 23,
    showPreHeaderPanel: true,
    createPreHeaderPanel: true,
    forceFitColumns: false,
    editable: true,
    //enableAddRow: true,
    asyncEditorLoading: false,
    alwaysShowVerticalScroll: true, // this is necessary when using Grid Menu with a small dataset
    rowHeight: 30,
     // gridMenu Object is optional
    // when not passed, it will act as a regular Column Picker (with default Grid Menu image of drag-handle.png)
   /* gridMenu: {
      menuUsabilityOverride: function (args) {
        // we could disable the menu entirely by returning false
        return false;
      },
      customTitle: "Custom Menus",
      columnTitle: "Columns",
      hideForceFitButton: false,
      hideSyncResizeButton: false,
      iconImage: "static/js/SlickGrid-2.4.15/images/drag-handle.png", // this is the Grid Menu icon (hamburger icon)
      iconCssClass: "fa fa-bars",    // you can provide iconImage OR iconCssClass
      leaveOpen: false,                 // do we want to leave the Grid Menu open after a command execution? (false by default)
      // menuWidth: 18,                 // width that will be use to resize the column header container (18 by default)
      resizeOnShowHeaderRow: true,
      customItems: [
        {
          iconImage: "static/js/SlickGrid-2.4.15/images/delete.png",
          title: "Clear Filters",
          disabled: false,
          command: "clear-filter",
          cssClass: 'bold',     // container css class
          textCssClass: 'red'   // just the text css class
        },
        {
          iconImage: "static/js/SlickGrid-2.4.15/images/info.gif",
          title: "Toggle Filter Row",
          disabled: false,
          command: "toggle-filter",
          itemUsabilityOverride: function (args) {
            // for example disable the toggle of the filter bar when there's filters provided
            return isObjectEmpty(columnFilters);
          },
        },
        {
          iconImage: "static/js/SlickGrid-2.4.15/images/info.gif",
          title: "Toggle Top Panel",
          disabled: false,
          command: "toggle-toppanel",
          cssClass: 'italic',     // container css class
          textCssClass: 'orange'  // just the text css class
        },
        // you can pass divider as a string or an object with a boolean
        // "divider",
        { divider: true },
        {
          iconCssClass: "icon-help",
          title: "Help",
          command: "help",
          textCssClass: "blue",
          // you could dynamically remove a command from the list (only checks before opening the menu)
          // for example don't show the "Help" button if we have less than 5 columns left
          itemVisibilityOverride: function (args) {
            return args.visibleColumns.length > 4;
          },
          action: function(e, args) {
            // you can use the "action" callback and/or subscribe to the "onCallback" event, they both have the same arguments
            console.log('execute an action on Help', args);
          }
        },
        {
          iconImage: "",
          title: "Disabled Command",
          disabled: true,
          command: "custom-command"
        }
      ]
    }*/
  };

  /*
  var columns = [{ id: 'id', field: 'id', name: '#', width: 40, excludeFromGridMenu: true }];
  var columnFilters = {};
  for (var i = 0; i < 10; i++) {
    columns.push({
      id: i,
      name: String.fromCharCode("A".charCodeAt(0) + i),
      field: i,
      width: 60
    });
      };
*/
function PopulateSelect(select, dataSource, addBlank) {
    var index, len, newOption;
    if (addBlank) { select.appendChild(new Option('', '')); }
    $.each(dataSource, function (value, text) {
         newOption = new Option(text, value);
        select.appendChild(newOption);
   });

};

function Select2Editor(args) {
    var $input;
    var defaultValue;
    var scope = this;
    var calendarOpen = false;
    this.keyCaptureList = [ Slick.keyCode.UP, Slick.keyCode.DOWN, Slick.keyCode.ENTER ];
    this.init = function () {
        $input = $('<select></select>');
        $input.width(args.container.clientWidth + 3);
        PopulateSelect($input[0], args.column.dataSource, true);
        $input.appendTo(args.container);
        $input.focus().select();

        $input.select2({
            placeholder: '-',
            allowClear: false   
        });
    };
    this.destroy = function () {
        $input.select2('close');
        $input.select2('destroy');
        $input.remove();
    };
    this.show = function () {
    };
    this.hide = function () {
    };
    this.position = function (position) {
    };
    this.focus = function () {
        $input.select2('input_focus');
    };
    this.loadValue = function (item) {
        defaultValue = item[args.column.field];
        $input.val(defaultValue);
        $input[0].defaultValue = defaultValue;
        $input.trigger("change.select2");
    };
    this.serializeValue = function () {
        return $input.val();
    };
    this.applyValue = function (item, state) {
        item[args.column.field] = state;
    };
    this.isValueChanged = function () {
        return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
    };
    this.validate = function () {
        return {
            valid: true,
            msg: null
        };
    };
    this.init();
}

function Select2Formatter(row, cell, value, columnDef, dataContext) {
    return columnDef.dataSource[value] || '-';
}
function dayFormatter(row, cell, value, columnDef, dataContext) {
      return value + ' days';
  }
function dateFormatter(row, cell, value, columnDef, dataContext) {
      return value.getMonth() + '/' + value.getDate() + '/' + value.getFullYear();
  }

function formatter(row, cell, value, columnDef, dataContext) {
      return value;
  }

function requiredFieldValidator(value) {
    if (value == null || value == undefined || !value.length||!value.trim().length) {
      return {valid: false, msg: "This is a required field"};
    } else {
      return {valid: true, msg: null};
    }
  }
function set_formatter() {
    for (i in columns){
    if ('datatype' in columns[i])
        {
            if (columns[i]['datatype']=='text')
            {
                columns[i]["formater"]=formatter;
                columns[i]["editor"]=Slick.Editors.Text;
                if (columns[i]["validator"]=='requiredFieldValidator')
                   columns[i]["validator"]=requiredFieldValidator;
            }
            //else if (columns[i]['datatype']=='date')
            //{
            //    columns[i]["formater"]=dateFormatter;
           // }
             else if (columns[i]['datatype']=='combo')
             {
                        columns[i]["editor"]=Select2Editor;
                        columns[i]["formatter"]= Select2Formatter;

               }
               else if (columns[i]['datatype']=='date')
               {
                   columns[i]["formater"]=dateFormatter;
                   columns[i]["editor"]=Slick.Editors.Date;
                   columns[i]["validator"]=requiredFieldValidator;
                   columns[i]["cssClass"]="cell-title";

               }
        }
    }
}

function filter(item) {
    for (var columnId in columnFilters) {
    console.log("col filter id: ", columnId);
      if (columnId !== undefined && columnFilters[columnId] !== "") {
        var c = slickgrid.getColumns()[slickgrid.getColumnIndex(columnId)];
        console.log("C: ",item[c.field],", Value: " , columnFilters[columnId]);
        if (!item[c.field].includes(columnFilters[columnId])){
        //if (item[c.field] != columnFilters[columnId]) {
          return false;
        }
      }
    }
    return true;
  }

function load_from_file()
{
//load the strains metadata from a text file
}

function export_to_text_file()
 {
 // get the strain/s metadate from the grid
  var f_name=$("#efname").val();

 if (f_name.split(".").length==0)
    f_name=f_name=".txt";


 var len=dataView.getLength();
      //console.log ("columns"+ JSON.stringify(columns));
      var lines=[]
     var data_to_sent=dataView.getItems()
     var headers=[];
     for (col in columns)
     {
     if (columns[col]['field']=="sel")
        continue;
     headers.push(columns[col]['field']);
     }
     //console.log("Header: "+headers.join('\t'));
     lines.push(headers.join("\t"));
     var len=dataView.getLength();
     for (var i = 0; i < len; i++)
     {
         // console.log (JSON.stringify(dataView.getItem(i)));

     var line=[];
     for (j in headers)
     {
         var value=dataView.getItem(i)[headers[j]];

         if (value == null || value == undefined || !value.length||!value.trim().length)
          line.push("");
         else
           line.push(value+"");
     {

     }
     }
     lines.push(line.join("\t"));

     }
     console.log(len+", ");
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(lines.join("\n")));
    element.setAttribute('download', f_name);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
   $("#efname").val("");
   $('#export_import_dialoug').modal('hide');

 }

function load_from_file()
{
var input = document.getElementById('fileimporter');
var file_ = input.files[0];
var text_='';

        if (file_) {
          var r = new FileReader();
          r.onload = function(e) {
              var contents = e.target.result;
              /*
            alert( "Got the file.\n"
                  +"name: " + file_.name + "\n"
                  +"type: " + file_.type + "\n"
                  +"size: " + file_.size + " bytes\n"
                  + "starts with: " + contents.substr(1, contents.indexOf("\n"))
            );
            */
            var contents=contents.split("\n");
            console.log("Length: "+contents.length);
            var header=contents[0].split("\t");
            var new_data=[];
            for (var i =1; i< contents.length; i++)
            {

            var line= contents[i].split("\t");
              if (line.length!=header.length)
                continue;

             var row={"id": dataView.getLength()+1+i};
             new_data.push(row);

            for (var j = 0; j < header.length; j++) {
               row[header[j]]=line[j];
            }
            }
              console.log(header.length+", contents::"+new_data.length);
              dataView.beginUpdate();
              dataView.setItems(new_data);
              dataView.endUpdate();
              dataView.refresh();
              slickgrid.invalidate();
            // for (s in new_data)
             //{
              //   dataView.insertItem(dataView.getLength(), new_data[s]);
               //  slickgrid.invalidate();
                // slickgrid.render();
            // }
                      input.value='';

          }
          r.readAsText(file_);

        } else {
          alert("Failed to load file");
        }

}

function toggleGridMenu(e)
  {
    //gridMenuControl.showGridMenu(e);
  }

function add_new_row ()
 {
     var dd = slickgrid.getData();
     item = {
          "id": dataView.getLength()+1
        };
     dataView.insertItem(dataView.getLength(), item);
     $("#submit_metadata").prop("disabled", true);
     slickgrid.invalidate();
      slickgrid.render();
      //var divHeight = $('#slickgrid').css('height');
      //console.log(divHeight);
      //divHeight=divHeight+40;
      //console.log(divHeight);

      //$('#slickgrid').css('height', 180+'px');
      }

function delete_row(){
    var selectedIndexes = slickgrid.getSelectedRows().sort().reverse();
    if (slickgrid.getSelectedRows().length ==0)
        return;
        var result = confirm("Are you sure you want to delete " + slickgrid.getSelectedRows().length + " row(s)?");
    if (result) {

      $.each(selectedIndexes, function (index, value) {
        var item = dataView.getItem(value); //RowNum is the number of the row
        if (item)
        {
        console.log(session_read_files.length+", "+group_reads.length);
        console.log(item.read_files+"::::::::"+session_read_files[0]);
        if (group_reads.indexOf(item.read_files)>-1)
        {
            group_reads.splice(group_reads.indexOf(item.read_files),1);

         var r_files = item.read_files.split(',');
         for (i in r_files)
         {
         console.log(i+": "+ r_files[i]);
         if  (session_read_files.indexOf(r_files[i].trim() )>-1)
               session_read_files.splice(session_read_files.indexOf(r_files[i].trim() ),1);
         }
         }
                   console.log(session_read_files.length+", "+group_reads.length);

          dataView.deleteItem(item.id); //RowID is the actual ID of the row and not the row number
        }
        else
        console.log("item is null"+item);
      });
      slickgrid.invalidate();
      slickgrid.render();
    }
   //unselect all selected rows if any
    slickgrid.getSelectionModel().setSelectedRanges([]);
  }

function syncGridSelection(slickgrid, preserveHidden) {
  var self = this;
  var selectedRowIds = self.mapRowsToIds(slickgrid.getSelectedRows());;
  var inHandler;

  function update() {
    if (selectedRowIds.length > 0) {
      inHandler = true;
      var selectedRows = self.mapIdsToRows(selectedRowIds);
      if (!preserveHidden) {
        selectedRowIds = self.mapRowsToIds(selectedRows);
      }
      slickgrid.setSelectedRows(selectedRows);
      inHandler = false;
    }
  }

  slickgrid.onSelectedRowsChanged.subscribe(function(e, args) {
    if (inHandler) { return; }
    selectedRowIds = self.mapRowsToIds(slickgrid.getSelectedRows());
  });

  this.onRowsChanged.subscribe(update);

  this.onRowCountChanged.subscribe(update);
}

function syncGridCellCssStyles(slickgrid, key) {
  var hashById;
  var inHandler;

  // since this method can be called after the cell styles have been set,
  // get the existing ones right away
  storeCellCssStyles(slickgrid.getCellCssStyles(key));

  function storeCellCssStyles(hash) {
    hashById = {};
    for (var row in hash) {
      var id = rows[row][idProperty];
      hashById[id] = hash[row];
    }
  }

  function update() {
    if (hashById) {
      inHandler = true;
      ensureRowsByIdCache();
      var newHash = {};
      for (var id in hashById) {
        var row = rowsById[id];
        if (row != undefined) {
          newHash[row] = hashById[id];
        }
      }
      slickgrid.setCellCssStyles(key, newHash);
      inHandler = false;
    }
  }

  slickgrid.onCellCssStylesChanged.subscribe(function(e, args) {
    if (inHandler) { return; }
    if (key != args.key) { return; }
    if (args.hash) {
      storeCellCssStyles(args.hash);
    }
  });

  this.onRowsChanged.subscribe(update);

  this.onRowCountChanged.subscribe(update);
}


function CreateAddlHeaderRow() {
    var $preHeaderPanel = $(slickgrid.getPreHeaderPanel())
        .empty()
        .addClass("slick-header-columns")
        .css('left','-1000px')
        .width(slickgrid.getHeadersWidth());
    $preHeaderPanel.parent().addClass("slick-header");
    var headerColumnWidthDiff = slickgrid.getHeaderColumnWidthDiff();
    var m, header, lastColumnGroup = '', widthTotal = 0;

    for (var i = 0; i < columns.length; i++) {
      m = columns[i];
      if (lastColumnGroup === m.columnGroup && i>0) {
        widthTotal += m.width;
        header.width(widthTotal - headerColumnWidthDiff)
      } else {
          widthTotal = m.width;
          header = $("<div class='ui-state-default slick-header-column' />")
            .html("<span class='slick-column-name'>" + (m.columnGroup || '') + "</span>")
            .width(m.width - headerColumnWidthDiff)
            .appendTo($preHeaderPanel);
      }
      lastColumnGroup = m.columnGroup;
    }
  }

////
function set_status_2 (e, args) {
    //var name=slickgrid.getColumns()[args.cell].id;
   // var val=data[args.row][slickgrid.getColumns()[args.cell].field];
    //var cell = $(slickgrid.getActiveCellNode());
    //if (val == null || val == ""|| val == "-"|| val == "")
    //    {
    //        return "red";
    //    }
     //    else {
     //    var cells=[cell];
         slickgrid.invalidateAllRows()
         slickgrid.invalidateRow(args.item.ID);
         slickgrid.render();
     //   }
}

function set_status (e, args) {
    //alert(slickgrid.getColumns()[args.cell].id);
    var name=slickgrid.getColumns()[args.cell].id;
    var val=args.value;
    if (required_fields.includes(name))
    {
        if (val == null || val == ""|| val == "-"|| val == "")
        {
            $("#submit_metadata").prop("disabled", true);
            return "red";
        }
         else
         {
                $("#submit_metadata").prop("disabled", false);

         }
    }

    }


//// set ` selector
//https://stackoverflow.com/questions/25804398/adding-file-upload-to-slickgrid-cell

function file_editor(args) {
var $input;
        var defaultValue;
        var scope = this;

        this.init = function () {
          $input = $("<INPUT type='file' class='editor-text' />")
              .appendTo(args.container)
              .bind("keydown.nav", function (e) {
                if (e.keyCode === $.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT) {
                  e.stopImmediatePropagation();
                }
              })
              .focus()
              .select();
        };

        this.destroy = function () {
          $input.remove();
        };

        this.focus = function () {
          $input.focus();
        };

        this.getValue = function () {
          return $input.val();
        };

        this.setValue = function (val) {
         //$input.val(val);
        };

        this.loadValue = function (item) {
          defaultValue = item[args.column.field] || "";
          $input.val(defaultValue);
          $input[0].defaultValue = defaultValue;
          $input.select();
        };

        this.serializeValue = function () {
          return $input.val();
        };

        this.applyValue = function (item, state) {
          item[args.column.field] = state;
        };

        this.isValueChanged = function () {
          return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
        };

        this.validate = function () {
          if (args.column.validator) {
            var validationResults = args.column.validator($input.val());
            if (!validationResults.valid) {
              return validationResults;
            }
          }

          return {
            valid: true,
            msg: null
          };
        };

        this.init();
      }
/////

function set_table(container)
{


const groupItemMetadataProvider = new Slick.Data.GroupItemMetadataProvider();

var columns_=[];
  set_formatter();
   checkboxSelector1 = new Slick.CheckboxSelectColumn({
      cssClass: "slick-cell-checkboxsel"
    });
    columns_.push(checkboxSelector1.getColumnDefinition());

    for (col in columns)
    {
    columns_.push(columns[col]);
    }


 columns=columns_;




  dataView = new Slick.Data.DataView({
    groupItemMetadataProvider: groupItemMetadataProvider,
    inlineFilters: true
});

  //var dataView = new Slick.Data.DataView({ inlineFilters: true });

    slickgrid = new Slick.Grid($('#'+container), dataView, columns, options);
     var pager = new Slick.Controls.Pager(dataView, slickgrid, $("#pager"));

     ////////////////////

    slickgrid.setSelectionModel(new Slick.CellSelectionModel());
    slickgrid.onAddNewRow.subscribe(function (e, args) {
      var item = args.item;
      slickgrid.invalidateRow(data.length);
      data.push(item);
      slickgrid.updateRowCount();
      slickgrid.render();
    });

     //////////////////////
    slickgrid.setSelectionModel(new Slick.RowSelectionModel({selectActiveRow: false}));
    slickgrid.registerPlugin(checkboxSelector1);
    var columnpicker = new Slick.Controls.ColumnPicker(columns, slickgrid, options);
    slickgrid.onSelectedRowsChanged.subscribe(function (e, args) {
        // debugging to see the active row in response to questions
        // active row has no correlation to the selected rows
        // it will remain null until a row is clicked and made active
        // selecting and deselecting rows via checkboxes will not change the active row
        var rtn = args.grid.getActiveCell();
        var x = args.rows;
    });



    dataView.onRowCountChanged.subscribe(function (e, args) {
          slickgrid.updateRowCount();
          slickgrid.render();
        });

    dataView.onRowsChanged.subscribe(function (e, args) {
          slickgrid.invalidateRows(args.rows);
          slickgrid.render();
        });

 $(slickgrid.getHeaderRow()).on("change keyup", ":input", function (e) {
      var columnId = $(this).data("columnId");
      if (columnId != null) {
        columnFilters[columnId] = $.trim($(this).val());
        dataView.refresh();
      }
    });
    /*
    slickgrid.onHeaderRowCellRendered.subscribe(function(e, args) {
        $(args.node).empty();
        $("<input type='text'>")
           .data("columnId", args.column.id)
           .val(columnFilters[args.column.id])
           .appendTo(args.node);
    });
*/
    //dataView.beginUpdate();
    //dataView.setItems(data);
    //dataView.setFilter(filter);
    //dataView.endUpdate();

slickgrid.onSort.subscribe(function (e, args) {
      var cols = args.sortCols;
      var data_=dataView.getItems();
      data_.sort(function (dataRow1, dataRow2) {
        for (var i = 0, l = cols.length; i < l; i++) {
          var field = cols[i].sortCol.field;
          var sign = cols[i].sortAsc ? 1 : -1;
          var value1 = dataRow1[field], value2 = dataRow2[field];
          var result = (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
          if (result != 0) {
            return result;
          }
        }
        return 0;
      });
      dataView.setItems(data_);
      slickgrid.invalidate();
      slickgrid.render();
    });

     slickgrid.onBeforeAppendCell.subscribe(set_status );
     slickgrid.onCellChange.subscribe(set_status_2);

    /*
    // subscribe to Grid Menu event(s)
    gridMenuControl.onCommand.subscribe(function(e, args) {
      if(args.command === "toggle-filter") {
        slickgrid.setHeaderRowVisibility(!slickgrid.getOptions().showHeaderRow);
      }
      else if(args.command === "toggle-toppanel") {
        slickgrid.setTopPanelVisibility(!slickgrid.getOptions().showTopPanel);
      }
      else if(args.command === "clear-filter") {
        $('.slick-headerrow-column').children().val('');
        for (var columnId in columnFilters) {
          columnFilters[columnId] = "";
        }
        dataView.refresh();
      } else {
        alert("Command: " + args.command);
      }
    });
    // subscribe to event when column visibility is changed via the menu
    gridMenuControl.onColumnsChanged.subscribe(function(e, args) {
      console.log('Columns changed via the menu');
    });
    // subscribe to event when menu is closing
    gridMenuControl.onMenuClose.subscribe(function(e, args) {
      console.log('Menu is closing');

    });
    slickgrid.onAutosizeColumns.subscribe(function(e, args) {
      console.log('onAutosize called')
    });
*/

    slickgrid.onColumnsResized.subscribe(function (e, args) {
      CreateAddlHeaderRow();
    });

    slickgrid.init();

    CreateAddlHeaderRow();
    // Initialise data
    dataView.beginUpdate();
    dataView.setItems(data);
    //dataView.setFilter(filter);
    dataView.endUpdate();


}