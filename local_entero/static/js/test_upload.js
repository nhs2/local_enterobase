var total_time=0;
var no_files=101;
var successful=0;
var failed=0;
var upload_jobs=[];
var is_it_run=false;


function upload_files(){

if (is_it_run==true)
    return;
  is_it_run=true;
$("#upload_progress_div").show();
//start_time=Date.now();
$("#upload_btn").hide();
$("#cancel_btn").show();
$("#button_div").hide();
$("#cancel_pargraph").hide();
//check if the base test file is created, if not create it
//AJax cll

var continu_=test_base_file();
if (continu_==true)
{
    var i;
    for (i = 1; i < no_files; i++) {
        test_upload_file(i);
    //set_progress_bar(i);
    }
    }
}

function cancel_upload_files()
{
//abort all the ajax cal
    for (i in upload_jobs){
        xpr=upload_jobs[i];
        xpr.abort();
        xpr = null;
    }
//adjust the page to allow starting the test if the user wants
    upload_jobs.length=0;
    $("#upload_progress_div").hide();
    is_it_run=false ;
    $("#upload_btn").show();
    $("#cancel_btn").hide();
    $("#button_div").show();
    $("#cancel_pargraph").show();
    $('#uploading_files_list').empty();
    successful=0;
    failed=0;
    set_progress_bar(0);
}

function test_base_file()
{

 var done=false;
$.ajax({
            type: "POST",
            url: "/check_base_upload_file",
            async: false,
            data: "{}", //
            dataType:'json',
            success: function (data) {
                  console.log("Results:",data) ;
                  done= true;

            },
            error : function(request,error)
    {
        console.log("Error: "+JSON.stringify(request));
        done= false;

    }
        });

return done;

}

function set_status_report(data, i)
{

    if (data['results']=="successful")
      {
         successful=successful+1;
         color='green';
      }
    else{
      failed=failed+1;
      color='red';
      }
      var counter=successful+failed;
      set_progress_bar(counter);
      var node = document.createElement("LI");
      var textnode = document.createTextNode("File upload test no. "+counter+" is "+data['results']);//+", elapsed time:"+data['elapsedTime']+" sec");
      node.appendChild(textnode);
      node.style.color = color;
      document.getElementById("uploading_files_list").appendChild(node);
      total_time=total_time+data['elapsedTime'];
      return counter;

}
function test_upload_file(i)
{

var xpr=$.ajax({
            type: "POST",
            url: "/test_upload_file",
            async: true,
            data: "{}", //
            dataType:'json',
            success: function (data) {
                  console.log("suc::",data) ; // display the returned data in the console.

                  counter= set_status_report(data,i);
                  if (counter==no_files-1)
                  {
                  var ave=total_time/counter
                  console.log("Average time: "+ave);
                  submit_test_results(counter, ave)
                  }
            },
            error : function(request,error)
    {
        console.log("Error, server response: "+JSON.stringify(request));
        failed=failed+1;

    }
        });
        upload_jobs.push(xpr);
}

function set_progress_bar(value){
    progress = document.getElementById("test_upload_files");
      if(value>=100) value = 100;


      progress.style.width = value + "%";
    }

function submit_test_results(no_uploaded_files, average_time)
{
$("#results_div").show();
                  $("#no_suc").append(successful);
                  $("#no_suc").css("color","green");
                  if (failed>0)
                  {
                   $("#no_fai").append(failed);
                  $("#no_fai").css("color","red");
                  }
                  else
                  $("#no_fai").hide();

                  $("#ave_time").append((average_time*no_uploaded_files).toFixed(2) +" seconds");
                  $("#uploaded_files_content").hide();
                  $("#cancel_pargraph").hide();
                  $("#cancel_btn").hide();

$.ajax({
            type: "POST",
            url: "/submit_test_upload_files",
            async: true,
            data: {'no_uploaded_files':no_uploaded_files,'average_time':average_time },
            dataType:'json',
            success: function (data) {
                  console.log("Uploading Data has been submitted: "+data) ;
                  },
            error : function(request,error)
    {
        console.log("Error: Request: "+JSON.stringify(request));

    }
        });
}