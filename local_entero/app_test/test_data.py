#data to be used to test the system
def get_test_data():
    data=[
        {'priority' : 1,'read_files' :'/home/khaled/temp/input/A_R1.fastq.gz, /home/khaled/temp/input/A_R2.fastq.gz',
             'user_id' : 1943, 'pipeline' : 'QAssembly', 'database' : 'dumy', 'version' : 1,
         },
        {'priority': -5, 'read_files': '/home/khaled/temp/input/A_R1.fastq.gz, /home/khaled/temp/input/A_R2.fastq.gz',
         'user_id': 1943, 'pipeline': 'QAssembly', 'database': 'dumy', 'version': 1,
         },
        {'priority': -6, 'read_files': '/home/khaled/temp/input/A_R1.fastq.gz, /home/khaled/temp/input/A_R2.fastq.gz',
         'user_id': 1943, 'pipeline': 'QAssembly', 'database': 'dumy', 'version': 1,
         },
        {'priority': -7, 'read_files': '/home/khaled/temp/input/A_R1.fastq.gz, /home/khaled/temp/input/A_R2.fastq.gz',
         'user_id': 1943, 'pipeline': 'QAssembly', 'database': 'dumy', 'version': 1,
         },
        {'priority': -8, 'read_files': '/home/khaled/temp/input/A_R1.fastq.gz, /home/khaled/temp/input/A_R2.fastq.gz',
         'user_id': 1943, 'pipeline': 'QAssembly', 'database': 'dumy', 'version': 1,
         }
        ]
    return data


def get_user():
   user ={
   "enterobase_id":1,
   "username":"warwick_user_name",
   "email":"ueremail@server.com"
   }
   return user

#the user_id is the same user id column, i.e. foreign column
def get_strain_data():
   strains_data={
   "strain":"DHP_2510_S130",
   "user_id":1,
   "database":"senterica",
   "release_period":12,
   "data":"{'seq_platform':'Illumina','seq_library':'Paired'}"
   }
   return strains_data

#the strain_data_id is the same strains_data id column, i.e. foreign column
def get_read_files():
   readfiles=[
   {
      "strain_data_id":2,
      "file_name":"DHP_2510_S130_merged_R1_001.fastq.gz",
      "file_location":"/home/user_name/local_enterobase_working_folder/input/1/senterica/DHP_2510_S130_merged_R1_001.fastq.gz",
      "database":"senterica",
      "user_id":1,
      "status":"Uploaded"
   },
   {
      "strain_data_id":2,
      "file_name":"DHP_2510_S130_merged_R2_001.fastq.gz",
      "file_location":"/home/user_name/local_enterobase_working_folder/input/1/senterica/DHP_2510_S130_merged_R2_001.fastq.gz",
      "database":"senterica",
      "user_id":1,
      "status":"Uploaded"
   }
   ]
   return readfiles
