'''
Purpose     : Create the application

| Revision History:
| Name:version 0.1
| Date: 02/2020
| Description: deployed
'''

import sys
import os
from flask import Flask
from flask_oauthlib.client import OAuth
from flask_bootstrap import Bootstrap
from logging.handlers import RotatingFileHandler
import logging
from flask import render_template
from config import configLooader
from celery import Celery
from celery.schedules import crontab
from flask_login import LoginManager
import local_entero.databases.databases_utilities as d_util
import local_entero.databases.databases_models as db_models
from flask_babel import Babel


from config import app_config as config_
config_.load_configuration_variables_from_file()

import ssl

app_config=config_


#create celery app
def make_celery(app, config):
    global celery
    celery = Celery(
        app.import_name,
        backend=config.CELERY_RESULT_BACKEND,
        broker=config.CELERY_BROKER_URL
    )
    app.config['CELERYBEAT_SCHEDULE'] = {
        # Executes every minute
        'periodic_task-every-minute': {
            'task': 'check_jobs_queue',
            'schedule': crontab(minute="*")
        }
    }
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery


#configure the app from config class
def create_app(config_name):
    global app_config
    if config_name=='development':
        # Disable SSL certificate verification,
        try:
            _create_unverified_https_context = ssl._create_unverified_context
        except AttributeError:
            pass
        else:
            # Handle target environment that doesn't support HTTPS verification
            ssl._create_default_https_context = _create_unverified_https_context
    app_config=configLooader.get(config_name)
    countries, continents_countries, continents=get_contirs_continents_info()
    local_app.config['COUNTRIES']=countries
    local_app.config['CONTINENTS_COUNTRIES'] = continents_countries
    local_app.config['CONTINENTS'] = continents
    app_config.set_database_variables(app_config.database)
    local_app.config.from_object(app_config)
    local_app.app_context()
    local_app.app_context().push()
    local_app.app_context()
    local_app.app_context().push()
    #bootstrap.init_app(local_app)
    # this will create the database and tables if they do not exist
    celery=make_celery(local_app, app_config)
    try:
        d_util.create_main_database(db, local_app, app_config)
        db_models.create_tables()
    except Exception as e:
        local_app.logger.info("Database error: %s, error is %s"%(sys.exc_info()[0], e))
        local_app.config.Error_message=local_app.config['Error_message']='Database error, please check system configuration'

    from local_entero.routes import routes as routers_blueprint
    local_app.register_blueprint(routers_blueprint, url_prefix='/')

    login_manager.init_app(local_app)
    login_manager.session_protection = None
    login_manager.login_view = 'routes.login'

    @login_manager.user_loader
    def load_user(user_id):
        print(user_id)
        try:
            return db_models.Users.query.get(int(user_id))
        except Exception as e:
            local_app.logger.error("User database, error: %s " % e)
            return None
    if (config_name=="production"):
        return local_app

    return local_app, celery

def create_warwick_connector (config_):
    config=config_.__dict__
    warwick_enterobase = oauth.remote_app(
            'warwick_enterobase',
            consumer_key=config.get('CLIENT_ID'),
            consumer_secret=config.get('CLIENT_SECRET'),
            request_token_params={
                'scope': 'Username, Email, Submitting Assemblies'
            },
            base_url=config.get('BASE_URL'),
            request_token_url=None,
            access_token_method='POST',
            access_token_url=config.get('ACCESS_TOKEN_URL'),
            authorize_url=config.get('AUTHORIZE_URL'),

        )
    return warwick_enterobase

local_app = Flask(__name__)
#hande not found error
@local_app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404

if not local_app.debug:
    # ...
    log_folder=os.path.join(os.path.expanduser('~'), 'logs')
    if not os.path.exists(log_folder):
        os.mkdir(log_folder)
    file_handler = RotatingFileHandler(os.path.join(log_folder, 'local_enteroBase_log.log'), maxBytes=100240,
                                       backupCount=10)
    file_handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
    file_handler.setLevel(logging.INFO)
    local_app.logger.addHandler(file_handler)

    local_app.logger.setLevel(logging.INFO)
    local_app.logger.info('local EnteroBase startup')

login_manager = LoginManager()
bootstrap = Bootstrap(local_app)

db = db_models.db

oauth = OAuth(local_app)
warwick_enterobase=create_warwick_connector(app_config)
babel = Babel(local_app)

def get_contirs_continents_info():
    try:
        with open(app_config.countries_data_file) as file:
            data = file.read()
        import json
        data=json.loads(data)
    except:
        data={}




    countries=[]
    continents=[]
    continents_countries={}
    for line in data:
        countries.append(line['country'])
        if line['continent'] not in continents:
            continents.append(line['continent'] )
        if line['continent'] not in continents_countries:
            conts=[]
            continents_countries[line['continent']]=conts
        else:
            conts=continents_countries[line['continent']]

        conts.append(line['country'])

    return countries, continents_countries, continents

    #print (line)
if __name__ == "__main__":
    local_app.run(host="127.0.0.1", port=5566, debug=True)

#upload_reads
#jobs_status