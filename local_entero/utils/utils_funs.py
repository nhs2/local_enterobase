import requests

def _connect_Warwick_get_resp(url, data_):
    resp = requests.post(url, data=data_, verify=False)
    return resp.text

def is_this_client_registered(token , BASE_URL_2 ):
    '''
    Check if the client submitted a registration request by
    checking token in the app configuration with Warwick EnteroBase
    :return:
    '''
    try:
        url = BASE_URL_2 + '/oauth/is_registerd_request_submitted'
        data_={'token':token}
        results=_connect_Warwick_get_resp(url, data_)
    except Exception as e:
        print ("errro in is_this_client_registered, error: %s"%e)
        results='False'
    return results