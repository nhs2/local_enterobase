"""
Purpose     :  Crate teh applcation configuration

SPECIAL NOTES: the configuration file is saved in user home folder, $HOME/.local_configuration_file.yml


| Revision History:
| Name:version 0.1
| Date: 02/2020
| Description: deployed in DSM EnteroBase

"""

import os
from shutil import copyfile
import yaml
from local_entero.utils.utils_funs import  is_this_client_registered

def check_create_folder(path):
    try:
        if not os.path.exists(path):
            os.makedirs(path)
    except:
        pass

def get_key(home_folder):
    encrypt_key_file=os.path.join(home_folder,'.encrypt_key.txt')
    if not os.path.isfile(encrypt_key_file):
        from cryptography.fernet import Fernet
        encrypt_key= Fernet.generate_key()
        with open(encrypt_key_file, 'wb') as pt:
            pt.write(encrypt_key)
    else:
        with open(encrypt_key_file, 'rb') as fh:
            encrypt_key= fh.read()
    return encrypt_key


def read_app_pass(home_folder):
    app_pass_file = os.path.join(home_folder, '.app_pass.txt')
    if  os.path.isfile(app_pass_file):
        with open(app_pass_file, 'rb') as fh:
            app_pass= fh.read()
        return app_pass

def write_app_pass(home_folder, password):
    from local_entero.routes.routes_utility_methods import encrypt_text
    app_pass_file = os.path.join(home_folder, '.app_pass.txt')

    e_app_pass=encrypt_text(password)
    with open(app_pass_file, 'wb') as pt:
        pt.write(e_app_pass)

def load_config_from_file(LOCAL_INSTANCE_CONFIG, config):
    print ("Injecting config variables from :%s"%LOCAL_INSTANCE_CONFIG)
    write_file = False
    with open(LOCAL_INSTANCE_CONFIG) as f:
        cofg = yaml.load(f)
    for x, y in cofg.items():
        setattr(config, x, y)

    if hasattr(config, 'base_folder'):
        config.BASE_FOLDER=config.base_folder
        cofg['BASE_FOLDER'] = config.BASE_FOLDER
        del cofg ['base_folder']
        write_file=True

    if not config.BASE_FOLDER:
        config.BASE_FOLDER=(os.path.join(os.path.expanduser('~')))+ r'/local_enterobase'
        cofg['BASE_FOLDER']=config.BASE_FOLDER
        write_file=True

    if not hasattr(config, 'INSTANCE_ICON'):
    #if has config.Instance_Icon:
        config.INSTANCE_ICON = os.path.join(os.path.expanduser('~'), 'salmonella.jpg')
        cofg['INSTANCE_ICON'] = config.INSTANCE_ICON
        copyfile(config.LOCAL_IMG_FILE, config.INSTANCE_ICON)
        write_file = True

    if not hasattr(config, 'CLIENT_TOKEN'):
        config.CLIENT_TOKEN=config.CLIENT_ID
        cofg['CLIENT_TOKEN']=config.TOKEN
        write_file = True

    if not hasattr(config,'HAS_BEEN_TESTED'):
        config.HAS_BEEN_TESTED = 'No'
        cofg['HAS_BEEN_TESTED'] = config.HAS_BEEN_TESTED
        write_file = True

    if hasattr(config, 'instance_name'):
        config.INSTANCE_NAME=config.instance_name
        cofg['INSTANCE_NAME']=config.INSTANCE_NAME
        del cofg['instance_name']
        write_file = True

    if hasattr(config,'POSTGRES_PASS'):
        config.DATABASE_PASSWORD=config.POSTGRES_PASS
        cofg['DATABASE_PASSWORD']=config.POSTGRES_PASS
        del cofg['POSTGRES_PASS']
        write_file = True

    if hasattr(config,'POSTGRES_SERVER'):
        config.DATABASE_SERVER_URI= config.POSTGRES_SERVER
        cofg['DATABASE_SERVER_URI']=config.POSTGRES_SERVER
        del cofg['POSTGRES_SERVER']
        write_file = True

    if (write_file):
        # check if something is changed in the config file
        # if so, save the  file and create backup
        copyfile(LOCAL_INSTANCE_CONFIG, LOCAL_INSTANCE_CONFIG + ".back")
        # write the new config to the configuration file
        with open(LOCAL_INSTANCE_CONFIG, 'w') as outfile:
            yaml.dump(cofg, outfile, default_flow_style=False)

    config.ACTIVE_FOLDER = config.BASE_FOLDER + r'/data'
    check_create_folder(config.ACTIVE_FOLDER)
    config.LOCAL_UPLOAD_DIR = config.ACTIVE_FOLDER + r'/local_upload_dir'
    check_create_folder(config.LOCAL_UPLOAD_DIR)
    config.WORKING_FOLDER = config.ACTIVE_FOLDER + r'/temp'
    check_create_folder(config.WORKING_FOLDER)
    config.OUTPUT_FOLDER = config.ACTIVE_FOLDER + r'/output'
    check_create_folder(config.OUTPUT_FOLDER)
    #ToDO The EToki singularity version needs to be configured
    #The following two configuration items are used localy for tesing the code
    ##########################################################################
    #The follwoing two items was used for testing purpose
    #It is needed to customize that  when integrate EToKi Singularity
    config.ASSEMBLY_COMMAND=os.path.join(os.path.expanduser('~'),'/EToKi/EToKi.py')
    config.ETOKI_PYTHON = os.path.join(os.path.expanduser('~'),'/entero/bin/python')
    ##########################################################################

    config.DATABASE = 'POSTGRESQL'
    #to be changed later
    try:
        IS_RESGISTERED=is_this_client_registered(config.CLIENT_TOKEN,config.BASE_URL_2)
    except Exception as e:
        print (e)
        IS_RESGISTERED='False'

    if IS_RESGISTERED   =='False':
        IS_RESGISTERED=False
    else:
        IS_RESGISTERED=True


    config.IS_RESGISTERED=IS_RESGISTERED


class app_config (object):
    LOCAL_CONFIG_FILE = os.path.join(os.path.abspath(os.path.dirname(__file__)),'local_entero/.local_configuration_file.yml')
    LOCAL_IMG_FILE = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                     'local_entero/static/img/salmonella.jpg')
    home_folder = os.path.expanduser('~')
    ENCRYPT_KEY=get_key(home_folder)
    APP_PASSWORD=read_app_pass(home_folder)
    write_file=False
    Error_message=''
    NEED_RESTART=False
    DATABASE= "POSTGRESQL"
    #SESSION_COOKIE_SECURE=True
    LOCAL_INSTANCE_CONFIG = os.path.join(home_folder, '.local_configuration_file.yml')
    if not os.path.isfile(LOCAL_INSTANCE_CONFIG):
        copyfile( LOCAL_CONFIG_FILE, LOCAL_INSTANCE_CONFIG)

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    countries_data_file = "local_entero/resources/contries_cont.txt"

    PREFERRED_URL_SCHEME: "https"
    REQUEST_CLIENT_REGISTER= 'https://enterobase.warwick.ac.uk/oauth/client_register_request'
    CHECK_CLIENT_REGISTER= 'https://enterobase.warwick.ac.uk/oauth/check_client_register'
    REQUEST_TOKEN_URL= 'https://enterobase.warwick.ac.uk/oauth/token'
    ACCESS_TOKEN_URL= 'https://enterobase.warwick.ac.uk/oauth/token'
    AUTHORIZE_URL= 'https://enterobase.warwick.ac.uk/oauth/authorize'
    BASE_URL= 'https://enterobase.warwick.ac.uk'
    BASE_URL_2 = 'http://enterobase.warwick.ac.uk'
    TEST_UPLOADS_ADDRESS = 'https://enterobase.warwick.ac.uk/upload/test_upload_assembly_file'

    ALLOWED_Sequencing_Platform=("illumina")
    ALLOWED_Sequencing_Library=("paired")
    SQLALCHEMY_TRACK_MODIFICATIONS=False
    Enterobase_Discovery_URL = (
        "https://enterobase.warwick.ac.uk:5001/client-configuration"
    )



    @staticmethod
    def load_configuration_variables_from_file():
        #loading application configuration variable from a file
        load_config_from_file(app_config.LOCAL_INSTANCE_CONFIG, app_config)

    @staticmethod
    def set_database_variables(database):
        # set the database attributes using configuration class
        # e.g. up database name and uri
        if app_config.DATABASE=='POSTGRESQL':
            if hasattr(app_config, 'DATABASE_PORT'):
                address = app_config.DATABASE_SERVER_URI + ':%s' % app_config.DATABASE_PORT
            else:
                address = app_config.DATABASE_SERVER_URI
            app_config.SQLALCHEMY_DATABASE_URI = 'postgresql://%s:%s@%s/%s' \
                                                 % (app_config.DATABASE_USER, \
                                                    app_config.DATABASE_PASSWORD, \
                                                    address,database)
            app_config.SQLALCHEMY_BINDS = dict(jobs_db=app_config.SQLALCHEMY_DATABASE_URI)

class development_app_config(app_config):
    database = 'local_database_develop'
    DEBUG = False
    VERIFY=False


class production_app_config(app_config):
    database = 'local_database_production'
    DEBUG = False
    VERIFY = False


class test_app_config (app_config ):
    TESTING = True
    #WTF_CSRF_ENABLED = False
    LIVESERVER_PORT = 8943
    # Default timeout is 5 seconds
    LIVESERVER_TIMEOUT = 10
    database ="local_database_test"



configLooader = {
     'development': development_app_config,
    'testing': test_app_config,
    'production': production_app_config
}

