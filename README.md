# Local EnteroBase
The main purpose of local EnteroBase is to enable our partners (users) to submit their strain metadata, along with the read files which will be assembled locally.
If they are assembled sucessfully, the strains metadata along with the assembly files will be sent to Warwick Enterobase (enterobase.warwick.ac.uk).
## The main functions:
* Receive and check the strain metadata along with read files
* Assemble the read files locally
* Enable the users to check the progress of their assemblies.
* Send the resultant assembly files and strain metadata to Warwick Enterobase

For the full documentation, see https://local-enterobase.readthedocs.io/en/latest/


